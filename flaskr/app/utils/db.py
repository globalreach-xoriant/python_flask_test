import logging
import os

from flask import current_app as app
from typing import Dict, List

from flask import Flask, jsonify
from sqlalchemy import create_engine, Integer, Column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from psycopg2 import connect

LOG = logging.getLogger(__name__)

class DBConnection(object):
    Session = None

    @staticmethod
    def init_db():
        return sessionmaker(
            bind=DBConnection.make_engine(), 
            autocommit=True
        )

    @staticmethod
    def make_engine():
        db_url = "postgresql://{}:{}@{}:{}/{}".format(
            app.config["POSTGRES_USERNAME"],
            app.config["POSTGRES_PASSWORD"],
            app.config["POSTGRES_HOSTNAME"],
            app.config["POSTGRES_HOSTPORT"],
            app.config["POSTGRES_DBNAME"]
        )

        engine = create_engine(
            db_url,
            pool_size=app.config["SQLALCHEMY_POOL_SIZE"],        # default in SQLAlchemy
            max_overflow=app.config["SQLALCHEMY_MAX_OVERFLOW"],  # default in SQLAlchemy
            pool_timeout=app.config["SQLALCHEMY_POOL_TIMEOUT"],  # raise an error faster than default
        )
        print(engine)
        return engine

    @staticmethod
    def get_session():
        Session = DBConnection.init_db()
        return Session()