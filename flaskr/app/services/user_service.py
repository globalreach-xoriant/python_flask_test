import os
import json
import logging

from dictalchemy import utils
from flask_bcrypt import Bcrypt
from flaskr.app.utils.db import DBConnection
from flaskr.app.models.user_model import UserModel

LOG = logging.getLogger(__name__)


class UserService(object):

    def perform_login(self, user_name, user_pwd):
        user_info = {}
        bcrypt = Bcrypt()
        session = DBConnection.get_session()
        result = (
            session.query(UserModel)
            .filter(
                UserModel.user_name == user_name,\
            )
            .all()
        )

        if result:
            user_data = result[0]
            db_pwd = user_data.user_pwd
            if bcrypt.check_password_hash(db_pwd, user_pwd):
               user_info = utils.asdict(user_data)
        
        return user_info