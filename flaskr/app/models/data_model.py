import logging

from sqlalchemy import (
    func,
    Column,
    String,
    Integer,
    Boolean,
    DateTime,
    ForeignKey,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

LOG = logging.getLogger(__name__)


class UserData(Base):
    __tablename__ = 'user_data'

    id = Column(Integer, primary_key=True, autoincrement=True)
    data_file_name = Column(String, nullable=False)
    total_count = Column(Integer, nullable=False)
    success_count = Column(Integer, nullable=False)
    fail_count = Column(Integer, nullable=False)
    created_time = Column(DateTime, server_default=func.now())
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    user = relationship("UserModel", back_populates="user_data")


class UserDataDetails(Base):
    __tablename__ = 'user_data_details'

    id = Column(Integer, primary_key=True, autoincrement=True)
    data_year = Column(Integer, nullable=False)
    industry_aggregation_nzsioc = Column(String, nullable=False)
    industry_code_nzsioc = Column(String, nullable=False)
    industry_name_nzsioc = Column(String, nullable=False)
    units = Column(String, nullable=False)
    variable_code = Column(String, nullable=False)
    variable_name = Column(String, nullable=False)
    variable_category = Column(String, nullable=False)
    data_value = Column(Integer, nullable=False)
    industry_code_anzsic06 = Column(String, nullable=False)
    created_time = Column(DateTime, server_default=func.now())
    user_data_id = Column(Integer, ForeignKey("user_data.id"), nullable=False)
    user_data = relationship("UserDataModel", back_populates="user_data_details")