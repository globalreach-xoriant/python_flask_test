import logging

from sqlalchemy import (
    func,
    Column,
    String,
    Integer,
    Boolean,
    DateTime,
    ForeignKey,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

LOG = logging.getLogger(__name__)


class UserModel(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    user_name = Column(String, nullable=False)
    user_pwd = Column(String, nullable=False)
    is_active = Column(Boolean, default=True)
    created_time = Column(DateTime, server_default=func.now())