import logging

from http import HTTPStatus
from flask import request
from flask_restplus import fields, Resource
from flask import current_app as app
from flaskr.flask_app import api
from flaskr.app.services.user_service import UserService
from flaskr.app.utils.app_utils import parse_duration
from flask_jwt_extended import (
    create_access_token, 
    create_refresh_token, 
    jwt_required, 
    jwt_refresh_token_required, 
    get_jwt_identity, 
    get_raw_jwt
)

LOG = logging.getLogger(__name__)


@api.route("/datafiles")
class DatafileController(Resource):
    @token_required
    def get(self):
        pass
