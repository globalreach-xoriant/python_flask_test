import logging

from http import HTTPStatus
from flask import request
from flask_restplus import fields, Resource
from flask import current_app as app
from flaskr.flask_app import api
from flaskr.app.services.user_service import UserService
from flaskr.app.utils.app_utils import parse_duration
from flask_jwt_extended import (
    create_access_token, 
    create_refresh_token, 
    jwt_required, 
    jwt_refresh_token_required, 
    get_jwt_identity, 
    get_raw_jwt
)

LOG = logging.getLogger(__name__)

user_model = api.model(
    "UserModel",
    {
        "user_name": fields.String,
        "user_pwd": fields.String
    }
)


@api.route("/login")
class FlaskAuthenticate(Resource):
    @api.expect(user_model, validate=True)
    def post(self):
        user_name = request.json.get("user_name")
        user_pwd = request.json.get("user_pwd")
        user_service = UserService()
        user_data = user_service.perform_login(user_name, user_pwd)

        response_status = "SUCCESS"
        response_message = "User logged-in successfully"
        response_data = None
        status_code = HTTPStatus.OK

        if not user_data:
            response_status = "ERROR"
            response_message = "Invalid username / password"
            status_code = HTTPStatus.NOT_FOUND
        else:
            jwt_token = create_access_token(identity = user_data['user_name'])
            refresh_token = create_refresh_token(identity = user_data['user_name'])
            expires = parse_duration(app.config["JWT_EXPIRY"])
            full_name = f"{user_data['last_name']} {user_data['first_name']}"

            response_data = {
                "access_token": jwt_token,
                "refresh_token": refresh_token,
                "expires_in": str(int(expires.total_seconds())),
                "username": user_name,
                "name": full_name,
            }

        response = {
            "status": response_status,
            "message": response_message,
            "data": response_data
        }

        return response, status_code

