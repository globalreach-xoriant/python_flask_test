import os
import logging

from flask import (
    Flask,
    Blueprint,
    jsonify,
    current_app,
    request,
    Response,
    has_request_context,
)
from flask_jwt_extended.exceptions import NoAuthorizationError
from flask_restplus import Api
from flask_restplus.api import SwaggerView
from flask_restplus.apidoc import Apidoc
from werkzeug.middleware.proxy_fix import ProxyFix
from flask_jwt_extended import JWTManager

LOG = logging.getLogger(__name__)

flaskr_app = Blueprint("flaskr", __name__)
api = Api(flaskr_app, title="Flaskr API", version="1.0", validate=True)
flaskr_api = api

def make_flask_app(*args, **kwargs):
    flask_app = Flask(__name__)
    flask_app.wsgi_app = ProxyFix(flask_app.wsgi_app, x_proto=1, x_host=1)
    flask_app.config["DEBUG"] = True
    flask_app.config['RESTPLUS_VALIDATE'] = True
    flask_app.config["JWT_SECRET_KEY"] = "ILoveCarta123#"
    flask_app.config.from_pyfile(
        os.path.abspath(
            os.path.join(os.path.dirname(__file__), "config/application.cfg")
        )
    )
    url_prefix=flask_app.config["APPLICATION_URL_PREFIX"]
    flask_app.register_blueprint(flaskr_app, url_prefix=url_prefix)
    jwt = JWTManager(flask_app)

    return flask_app