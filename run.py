#!/usr/bin/env python
import functools
import json
import uuid
import logging

from flask_jwt_extended import JWTManager

LOG = logging.getLogger(__name__)


def main(flask_app_factory):
    flask_app = flask_app_factory()
    # LOG.info(f"Loaded flask app with {len(flask_app.url_map._rules)} routes")

    listen = flask_app.config["APPLICATION_HOST"]
    port = int(flask_app.config["APPLICATION_PORT"])
    debug = flask_app.config["APPLICATION_DEBUG"]

    LOG.info(
        "Starting server: listen={listen}, debug={debug}, port={port}".format(
            listen=listen,
            debug=debug,
            port=port,
        )
    )
    
    flask_app.run(
        host=listen, port=port, debug=debug, threaded=True
    )


if __name__ == "__main__":
    from flaskr.flask_app import make_flask_app
    main(make_flask_app)
